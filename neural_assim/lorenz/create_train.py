
# coding: utf-8

# In[ ]:


import os
from contextlib import redirect_stdout
import io
import argparse

from enkf_lorenz.models import Lorenz96
from enkf_lorenz.integrator import RK4Integrator
from enkf_lorenz.utilities import forward_model

import numpy as np
import xarray as xr


from tqdm import tqdm

import logging


logging.basicConfig(level=logging.WARNING)
logger = logging.getLogger(__name__)

rnd = np.random.RandomState(42)


# General time and grid settings
# Before this number of days the run will be used as initialization
start = 1000
# The timedelta in model time unit 0.05 ~ 6 hours
dt = 0.05
dt_days = dt*5
end = 20000+start
all_steps = np.arange(0, end+dt_days, dt_days)
nr_grids = 40
F = 8.0
ens_size = 50
ens_pert_std = 0.3
ens_ana_time = 5
ens_lead_time = 5


def main(base_path):
    # Generate VR1
    start_state = rnd.normal(0, 0.01, size=(1, nr_grids))
    l96_vr1 = Lorenz96(F, nr_grids)
    vr1_integrator = RK4Integrator(l96_vr1, dt=dt)

    ds_vr1 = forward_model(all_steps, start, start_state, vr1_integrator,
                           nr_grids=nr_grids)
    vr1_path = os.path.join(base_path, 'train_vr1.nc')
    ds_vr1.to_netcdf(vr1_path)

    # Generate Ensemble
    ens_f = rnd.normal(0, 0.5, size=(1, ens_size, 1)) + F
    l96_ensemble = Lorenz96(ens_f, nr_grids)
    ensemble_integrator = RK4Integrator(l96_ensemble, dt=dt)
    ens_fcst_steps = np.arange(0, ens_lead_time, dt_days)
    base_states = ds_vr1.sel(time=ds_vr1.time % ens_ana_time == 0)
    ensemble_data = []

    pbar = tqdm(total=len(base_states.time.values))

    f = io.StringIO()
    with redirect_stdout(f):
        for ana_time in base_states.time.values:
            tmp_state = rnd.normal(scale=0.5, size=(ens_size, 1, 40))
            tmp_state = tmp_state + base_states.sel(time=ana_time).values
            tmp_state = tmp_state.transpose(1, 0, 2)
            ens_fcst = forward_model(ens_fcst_steps, 0, tmp_state,
                                     ensemble_integrator, ens_mems=ens_size,
                                     nr_grids=nr_grids)
            ens_fcst_vals = ens_fcst.values
            ensemble_data.append(ens_fcst_vals)
            pbar.update()
    ensemble_data = np.array(ensemble_data).transpose(1, 0, 2, 3, 4)
    ensemble_coords = dict(ens_fcst.coords)
    ensemble_coords['analysis'] = base_states.time.values
    ensemble_ds = xr.DataArray(
        data=ensemble_data,
        coords=ensemble_coords,
        dims=['varname', 'analysis', 'ensemble', 'time', 'grid']
    )
    ens_path = os.path.join(base_path, 'train_ens.nc')
    ensemble_ds.to_netcdf(ens_path)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Create trainings data')
    parser.add_argument(
        '--path', metavar='p', type=str,
        default='/scratch/local1/Data/neural_nets/neural_assim/data/',
        help='Path where the data will be stored'
    )
    args = parser.parse_args()
    main(args.path)
