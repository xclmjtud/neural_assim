#!/bin/env python
# -*- coding: utf-8 -*-
#
# Created on 10/4/18
#
# Created for neural_assim
#
# @author: Tobias Sebastian Finn, tobias.sebastian.finn@uni-hamburg.de
#
#    Copyright (C) {2018}  {Tobias Sebastian Finn}
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# System modules
import logging
import abc

# External modules
import tensorflow as tf

# Internal modules


logger = logging.getLogger(__name__)


class BaseOperator(object):
    def __init__(self, sess, len_grid=40, random_state=None):
        """
        This is a BaseClass for observation operators. These observation
        operators are used to map a model state to observations.

        Parameters
        ----------
        sess : :py:class:`tensorflow.Session`
            If this observation operator is called, the graph will be added to
            this session.
        len_grid : int, optional
            Number of grid points in Lorenz '96 model. Default is 40.
        random_state : :py:class:`numpy.random.RandomState` or None, optional
            This random state can be used for random numbers. Default is None.
        """
        self._input = None
        self._output = None
        self.sess = sess
        self.len_grid = len_grid
        self.random_state = random_state

    def __call__(self, input_vals, *args, **kwargs):
        feed_dict = {self.input: input_vals}
        return self.sess.run(self.output, feed_dict=feed_dict, *args, **kwargs)

    @property
    def input(self):
        if self._input is None:
            self.compile_op()
        return self._input

    @property
    def output(self):
        if self._output is None:
            self.compile_op()
        return self._output

    @abc.abstractmethod
    def obs_ops(self, in_tensor):
        pass

    def compile_op(self):
        self._input = tf.placeholder(tf.float32, shape=(None, self.len_grid),
                                     name='input')
        self._output = self.obs_ops(self._input)
