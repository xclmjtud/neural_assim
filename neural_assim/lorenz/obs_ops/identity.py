#!/bin/env python
# -*- coding: utf-8 -*-
#
# Created on 10/4/18
#
# Created for neural_assim
#
# @author: Tobias Sebastian Finn, tobias.sebastian.finn@uni-hamburg.de
#
#    Copyright (C) {2018}  {Tobias Sebastian Finn}
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# System modules
import logging

# External modules
import numpy as np

import tensorflow as tf

from sacred import Ingredient

# Internal modules
from .base_ops import BaseOperator


logger = logging.getLogger(__name__)


obs_ingredient = Ingredient('obs')


@obs_ingredient.config
def config():
    obs_points = [0, 5, 10, 15, 20, 25, 30, 35]


class ObsOperator(BaseOperator):
    @obs_ingredient.capture
    def __init__(self, sess, obs_points=None, len_grid=40, random_state=None):
        """
        This linear observation operator is an identity observation operator,
        where observed grid points equal observations.

        Parameters
        ----------
        sess : :py:class:`tensorflow.Session`
            If this observation operator is called, the graph will be added to
            this session.
        obs_points : list(int), int or None
            Observed grid points. If this is int, then this number of grid
            points are drawn from grid. If this is a list, observed grid points
            are prescribed. If this is None, all grid points are observed.
            Default is None.
        len_grid : int, optional
            Number of grid points in Lorenz '96 model. Default is 40.
        random_state : :py:class:`numpy.random.RandomState` or None, optional
            This random state can be used for random numbers. Default is None.
        """
        super().__init__(sess, len_grid=len_grid, random_state=random_state)
        self.obs_points = obs_points

    def obs_ops(self, in_tensor):
        if isinstance(self.obs_points, int):
            obs_inds = self.random_state.choice(self.len_grid,
                                                size=self.obs_points,
                                                replace=False)
            out_tensor = self._list_obs(in_tensor, obs_inds)
        elif isinstance(self.obs_points, list):
            out_tensor = self._list_obs(in_tensor, self.obs_points)
        else:
            out_tensor = self._all_obs(in_tensor)
        return out_tensor

    def _list_obs(self, in_tensor, obs_points):
        observed_points = np.zeros(shape=self.len_grid, dtype=bool)
        observed_points[obs_points] = True
        transposed_state = tf.transpose(in_tensor)
        obs_state = tf.transpose(
            tf.boolean_mask(transposed_state, observed_points)
        )
        return obs_state

    @staticmethod
    def _all_obs(in_tensor):
        out_tensor = tf.identity(in_tensor, name='observed_state')
        return out_tensor
