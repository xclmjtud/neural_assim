#!/bin/env python
# -*- coding: utf-8 -*-
#
# Created on 10/2/18
#
# Created for neural_assim
#
# @author: Tobias Sebastian Finn, tobias.sebastian.finn@uni-hamburg.de
#
#    Copyright (C) {2018}  {Tobias Sebastian Finn}
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# System modules
import logging
import os

# External modules
import numpy as np

import matplotlib.pyplot as plt
import matplotlib.gridspec as mpl_gridspec

# Internal modules


logger = logging.getLogger(__name__)


def plot_generator(model, obs_op, data, curr_epoch, log_path, _run, _rnd):
    """
    Plot generator, including comparison plot for all ensemble members between
    background, analysis and difference (contourf_*.png).
    Additional ensemble mean plot in state (prior_mean_*.png) and observation
    space (obs_mean_*.png). The plots are saved under:
    ``log_path/pictures/exp_name + time``.
    """
    _, _, _, valid_ens, valid_obs, _ = data
    plot_path = os.path.join(
        log_path, 'pictures', '{0:s}_{1:s}'.format(
            _run.experiment_info['name'], _run.start_time.strftime('%Y%m%d%H%M')
        )
    )
    if not os.path.exists(plot_path):
        os.makedirs(plot_path)
    rnd_enc_vals = _rnd.normal(size=(50, model.grid_size))
    rnd_dec_vals = _rnd.normal(size=(50, model.obs_size))
    rnd_idx = _rnd.randint(
        valid_ens.shape[0], size=1
    )
    drawn_ens = valid_ens[rnd_idx].squeeze().T
    drawn_obs = valid_obs[rnd_idx]
    obs_in = np.repeat(drawn_obs, 50, axis=0)

    analysed_ens, _ = model.encoder.predict([obs_in, drawn_ens, rnd_enc_vals])
    analysed_obs, _ = model.decoder.predict([analysed_ens, rnd_dec_vals])
    prior_obs, _ = model.decoder.predict([drawn_ens, rnd_dec_vals])

    fig, ax = plt.subplots(nrows=3, sharey=True, sharex=True)
    cf = ax[0].contourf(drawn_ens, vmin=-2, vmax=2)
    _ = ax[1].contourf(analysed_ens, vmin=-2, vmax=2)
    _ = ax[2].contourf(analysed_ens-drawn_ens, vmin=-2, vmax=2, cmap='coolwarm')
    ax[1].set_ylabel('Ensemble member')
    ax[1].set_xlabel('Grid point')
    plt.savefig('{0:s}/contourf_latest.png'.format(plot_path))
    plt.savefig('{0:s}/contourf_e{1:03d}.png'.format(plot_path, curr_epoch))
    plt.close('all')

    prior_mean = np.mean(drawn_ens, axis=0)
    analysis_mean = np.mean(analysed_ens, axis=0)
    gs = mpl_gridspec.GridSpec(4, 1)
    fig = plt.Figure()
    ax1 = plt.subplot(gs[:-1, :])
    ax1.plot(prior_mean, c='#1f77b4', label='prior', zorder=0)
    ax1.plot(analysis_mean, c='#ff7f0e', label='analysis',
             zorder=1)
    ax1.set_ylim(-3, 3)
    ax1.set_ylabel('Ensemble mean')
    ax1.legend(loc=3)
    ax2 = plt.subplot(gs[-1, :], sharex=ax1)
    ax2.axhline(y=0, xmin=0, xmax=99, c='skyblue')
    if 'obs_points' in _run.config['obs']:
        for obs_loc in _run.config['obs']['obs_points']:
            ax2.axvline(x=obs_loc, ymin=-1, ymax=1, c='0.5')
    ax2.plot(analysis_mean - prior_mean, c='black')
    ax2.set_ylabel('Difference in mean')
    ax2.set_ylim(-1.5, 1.5)
    ax2.set_yticks(ticks=(-1, 0, 1))
    ax2.set_xlabel('Grid point')
    ax2.set_xlim(0, 40)
    plt.subplots_adjust(hspace=0.0)
    plt.savefig('{0:s}/prior_mean_latest.png'.format(plot_path))
    plt.savefig('{0:s}/prior_mean_e{1:03d}.png'.format(plot_path, curr_epoch))
    plt.close('all')

    prior_obs_mean = np.mean(prior_obs, axis=0)
    analysis_obs_mean = np.mean(analysed_obs, axis=0)
    scaled_ana = analysis_mean * _run.info['ens.stddev'] + _run.info['ens.mean']
    ident_mean = (obs_op(scaled_ana[None, ...])-_run.info['obs.mean']) / \
                 _run.info['obs.stddev']
    gs = mpl_gridspec.GridSpec(4, 1)
    fig = plt.Figure()
    ax1 = plt.subplot(gs[:-1, :])
    if 'obs_points' in _run.config['obs']:
        ax1.scatter(_run.config['obs']['obs_points'], ident_mean,
                    c='#c60d0d', label='analysis_real', zorder=0, marker='o')
        ax1.scatter(_run.config['obs']['obs_points'], prior_obs_mean,
                    c='#1f77b4', label='prior', zorder=1, marker='o')
        ax1.scatter(_run.config['obs']['obs_points'], analysis_obs_mean,
                    c='#ff7f0e',
                    label='analysis', zorder=2, marker='o')
        ax1.scatter(_run.config['obs']['obs_points'], drawn_obs, c='black',
                    marker='x', label='observation', zorder=99)
    else:
        ax1.plot(ident_mean,
                 c='#c60d0d', label='analysis_real', zorder=0)
        ax1.plot(prior_obs_mean, c='#1f77b4',
                 label='prior', zorder=1)
        ax1.plot(analysis_obs_mean, c='#ff7f0e',
                 label='analysis', zorder=2)
        ax1.plot(drawn_obs, c='black',
                 label='observation', zorder=99)
    ax1.set_ylim(-3, 3)
    ax1.set_ylabel('Observations mean')
    ax1.legend(loc=3)
    ax2 = plt.subplot(gs[-1, :], sharex=ax1)
    ax2.axhline(y=0, xmin=0, xmax=99, c='skyblue')
    if 'obs_points' in _run.config['obs']:
        ax2.scatter(_run.config['obs']['obs_points'],
                    analysis_obs_mean - prior_obs_mean, c='black', marker='x')
    else:
        ax2.plot(analysis_obs_mean - prior_obs_mean, c='black', marker='x')
    ax2.set_ylabel('Difference in mean')
    ax2.set_ylim(-1.5, 1.5)
    ax2.set_yticks(ticks=(-1, 0, 1))
    ax2.set_xlabel('Grid point')
    ax2.set_xlim(0, 40)
    plt.subplots_adjust(hspace=0.0)
    plt.savefig('{0:s}/obs_mean_latest.png'.format(plot_path))
    plt.savefig('{0:s}/obs_mean_e{1:03d}.png'.format(plot_path, curr_epoch))
    plt.close('all')
