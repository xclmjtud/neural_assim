#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# System modules
from setuptools import setup, find_packages
from codecs import open
from os import path
import glob

# External modules

# Internal modules

__version__ = "0.1.0"

BASE = path.abspath(path.dirname(__file__))

try:
    import pypandoc
    long_description = pypandoc.convert(path.join(BASE, 'README.md'), 'rst')
except(IOError, ImportError):
    long_description = open(
        path.join(BASE, 'README.md'), encoding='utf-8').read()


setup(
    name='neural_assim',

    version=__version__,

    description='neural_assim is a package for data assimilation based on '
                'neural networks',
    long_description=long_description,


    author='Tobias Sebastian Finn',
    author_email='tobias.sebastian.finn@uni-hamburg.de',

    license='GPL3',

    keywords='deep learning data assimilation meteorology atmospheric science',

    packages=find_packages(exclude=['contrib', 'docs', 'tests.*', 'test']),
)
