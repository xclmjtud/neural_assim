#!/bin/env python
# -*- coding: utf-8 -*-
#
# Created on 18.06.18
#
# Created for neural_assim
#
# @author: Tobias Sebastian Finn, tobias.sebastian.finn@uni-hamburg.de
#
#    Copyright (C) {2018}  {Tobias Sebastian Finn}
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# System modules
import logging
import abc

# External modules
import tensorflow as tf

import keras.backend as k_backend

# Internal modules


logger = logging.getLogger(__name__)


class BaseModel(object):
    """
    A boilerplate to prototype effectively complicated deep learning
    architectures like Generative Adversarial Networks . This is build
    especially for TensorFlow and Keras. The main method is :py:attr:`compile`,
    where all involved models are composed.
    """
    def __init__(self, sess, summary_dir='/tmp/neural'):
        self._learning = 1
        self.session = sess
        self.summary_dir = summary_dir
        self.saver = None
        self.train_writer = None
        self.test_writer = None
        self.summary = None
        self.grads = []
        self.train_params = []
        self.train_steps = dict(gen=[], disc=[])
        self.losses = dict()
        self.metrics = dict()
        self.models = dict()

        # These attributes need to be set during initialization of the child
        self.inputs = dict()
        self.lam = dict()
        self.learning_rates = dict()
        self.optimizers = dict()

        # The assimilation node, which is used for testing purpose
        self.assim_node = None

    @property
    def learning(self):
        return self._learning

    @learning.setter
    def learning(self, phase):
        if phase != self._learning:
            k_backend.set_learning_phase(phase)
            self._learning = phase

    @abc.abstractmethod
    def compile(self):
        pass

    def compile_summary(self):
        with tf.name_scope('gradients'):
            for grad in self.grads:
                corrected_name = grad.name.replace(':', '_')
                tf.summary.histogram(corrected_name, grad)
        with tf.name_scope('weights'):
            for weight in self.train_params:
                corrected_name = weight.name.replace(':', '_')
                tf.summary.histogram(corrected_name, weight)
        with tf.name_scope('losses'):
            for name, loss in self.losses.items():
                corrected_name = name.replace(':', '_')
                tf.summary.scalar(corrected_name, loss)
        with tf.name_scope('metrics'):
            for name, loss in self.metrics.items():
                corrected_name = name.replace(':', '_')
                tf.summary.scalar(corrected_name, loss)
        self.summary = tf.summary.merge_all()
        self.train_writer = tf.summary.FileWriter(self.summary_dir + '/train',
                                                  self.session.graph)
        self.test_writer = tf.summary.FileWriter(self.summary_dir + '/test')

    def fit_on_batch(self, in_data):
        self.learning = 1
        feed_dict = {self.inputs[key]: value for key, value in in_data.items()}
        _ = self.session.run(
            fetches=self.train_steps['gen'],
            feed_dict=feed_dict
        )
        _ = self.session.run(
            fetches=self.train_steps['disc'],
            feed_dict=feed_dict
        )

    def evaluate_on_batch(self, in_data, summarize=False):
        self.learning = 0
        all_fetches = {
            'losses': self.losses,
            'metrics': self.metrics
        }
        feed_dict = {self.inputs[key]: value for key, value in in_data.items()}
        if summarize:
            summary, step_out = self.session.run(
                fetches=[self.summary, all_fetches],
                feed_dict=feed_dict
            )
            return summary, step_out
        else:
            step_out = self.session.run(
                fetches=all_fetches,
                feed_dict=feed_dict
            )
            return step_out

    def save_model(self, path, **kwargs):
        saved_path = self.saver.save(self.session, path, **kwargs)
        logger.info('Saved model to: {0:s}'.format(saved_path))

    @classmethod
    def load_model(cls, sess, path, **kwargs):
        ens_model = cls(sess)
        ens_model.compile()
        ens_model.saver.restore(sess, path, **kwargs)
        logger.info('Loaded model from: {0:s}'.format(path))
        return ens_model
