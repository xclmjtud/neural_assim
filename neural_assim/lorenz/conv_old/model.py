#!/bin/env python
# -*- coding: utf-8 -*-
#
# Created on 26.04.18
#
# Created for neural_assim
#
# @author: Tobias Sebastian Finn, tobias.sebastian.finn@uni-hamburg.de
#
#    Copyright (C) {2018}  {Tobias Sebastian Finn}
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# System modules
import logging

# External modules
import tensorflow as tf
import keras as k
import keras.layers as klayers
import keras.backend as K
from keras.layers.convolutional import _Conv
from keras.layers.merge import Average
from keras.engine import InputSpec

# Internal modules


logger = logging.getLogger(__name__)


def roll_pad(t, padding=(1, 1)):
    t_left = t[..., -padding[0]:, :]
    t_right = t[..., :padding[1], :]
    padded = tf.concat(
        [t_left[..., :padding[0], :], t, t_right[..., -padding[1]:, :]], axis=-2
    )
    return padded


class EnsConvLayer(_Conv):
    def build(self, input_shape):
        super().build(input_shape)
        self.input_spec = InputSpec(
            ndim=len(input_shape), axes={-1: input_shape[-1]}
        )


class LogVariance(Average):
    def _merge_function(self, inputs):
        mean = super()._merge_function(inputs=inputs)
        perts = [i-mean for i in inputs]
        output = tf.pow(perts[0], 2)
        for i in range(1, len(inputs)):
            output += tf.pow(perts[i], 2)
        var = output / len(inputs)
        log_var = tf.log(var)
        return log_var

class VAEMem(object):
    def __init__(self, session, ens_size=50, grid_points=40, latent_dim=1,
                 obs_dim=5, hidden_dims=None, summary_dir='/tmp/neural'):
        self._learning = None
        self._hidden_dims = None
        self.session = session
        self.summaries_dir = summary_dir
        self.summary = None
        self.saver = None
        self.train_writer = None
        self.test_writer = None
        self.encoder = None
        self.decoder = None
        self.single_encoder = None
        self.discriminator = None
        self.forward = None
        self.inverse = None
        self.hidden_dims = hidden_dims
        self.grid_points = grid_points
        self.latent_dim = latent_dim
        self.obs_dim = obs_dim
        self.ens_size = ens_size
        self.lam = {
            'kl': tf.Variable(
                0.1, trainable=False, name='lam_kl', dtype=tf.float32
            ),
            'rec': tf.Variable(
                10.0, trainable=False, name='lam_rec', dtype=tf.float32
            ),
            'trip': tf.Variable(
                10.0, trainable=False, name='lam_trip', dtype=tf.float32
            ),
            'gan': tf.Variable(
                1.0, trainable=False, name='lam_gan', dtype=tf.float32
            ),
        }
        self.began = {
            'lr': tf.Variable(
                0.0001, trainable=False, name='began_lr', dtype=tf.float32
            ),
            'div_ratio': tf.Variable(
                0.5, trainable=False, name='diversity_ratio', dtype=tf.float32
            ),
            'gain': tf.Variable(
                0, trainable=False, name='began_gain', dtype=tf.float32
            ),
        }
        self.learning_rate = {
            'ae': tf.Variable(0.001, dtype=tf.float32),
            'disc': tf.Variable(0.001, dtype=tf.float32),
            'obs': tf.Variable(0.001, dtype=tf.float32),
        }
        self.optimizers = {
            'ae': k.optimizers.Adam(amsgrad=True, lr=self.learning_rate['ae']),
            'disc': k.optimizers.Adam(amsgrad=True,
                                      lr=self.learning_rate['disc']),
            'obs': k.optimizers.Adam(amsgrad=True,
                                     lr=self.learning_rate['obs']),
        }
        self.inputs = dict(
            x_back_ens=klayers.Input(shape=(self.grid_points, self.ens_size)),
            x_back_mem=klayers.Input(shape=(self.grid_points,)),
            obs_in=klayers.Input(shape=(self.obs_dim, ))
        )
        self.grads = []
        self.train_params = []
        self.train_steps = []
        self.losses = {}
        self.metrics = {}

    def init_models(self):
        self.single_encoder = self.get_single_encoder()
        self.encoder = self.get_encoder()
        self.decoder = self.get_decoder()
        self.discriminator = self.get_discriminator()
        self.forward = self.get_forward()
        self.inverse = self.get_inverse()

    @property
    def hidden_dims(self):
        return self._hidden_dims

    @hidden_dims.setter
    def hidden_dims(self, new_dims):
        if new_dims is None:
            self._hidden_dims = [64, ]
        else:
            self._hidden_dims = new_dims

    @property
    def learning(self):
        return self._learning

    @learning.setter
    def learning(self, phase):
        if phase != self._learning:
            K.set_learning_phase(phase)
            self._learning = phase

    @staticmethod
    def sampling(args):
        z_mean, z_log_var = args
        epsilon = tf.random_normal(
            shape=tf.shape(z_log_var), mean=0., stddev=1, seed=42
        )
        return tf.exp(z_log_var / 2) * epsilon + z_mean

    @staticmethod
    def conv_layer(in_layer, filters, kernel_size=3, strides=1,
                   activation='relu', batch_norm=True):
        pad_size = int((kernel_size - 1) / 2)
        out_layer = klayers.Lambda(
            lambda t: roll_pad(t, (pad_size, pad_size))
        )(in_layer)
        out_layer = klayers.Conv1D(
            filters=filters, kernel_size=kernel_size, strides=strides
        )(out_layer)
        if batch_norm:
            out_layer = klayers.BatchNormalization()(out_layer)
        if isinstance(activation, str):
            out_layer = klayers.Activation(activation)(out_layer)
        return out_layer

    def get_single_encoder(self):
        enc_hidden = in_layer = klayers.Input(shape=(self.grid_points, 1))
        for n_dim, dim in enumerate(self.hidden_dims):
            if n_dim > 0:
                enc_hidden = self.conv_layer(enc_hidden, dim, strides=2)
            else:
                enc_hidden = self.conv_layer(enc_hidden, dim)
            enc_hidden = self.conv_layer(enc_hidden, dim)
        latent_vector = self.conv_layer(enc_hidden, 1, activation=None,
                                        batch_norm=False)
        single_model = k.Model(inputs=in_layer, outputs=latent_vector)
        return single_model

    def get_encoder(self, in_layer):
        """
        The encoder is basically an encoder for a variational autoencoder, but
        but it takes the ensemble members as feature input.
        """
        with tf.variable_scope('encoder'):
            splitted_ens = klayers.Lambda(
                lambda x: tf.split(x, self.ens_size, axis=-1)
            )(in_layer)
            latent_vec = [self.single_encoder(t) for t in splitted_ens]
            mean_vector = klayers.Flatten()(Average()(latent_vec))
            log_var_vector = klayers.Flatten()(LogVariance()(latent_vec))
            encoder_model = k.Model(
                inputs=in_layer, outputs=[mean_vector, log_var_vector]
            )
        return encoder_model

    def get_decoder(self, in_layer):
        with tf.variable_scope('decoder'):
            dec_hidden = klayers.Reshape(target_shape=(self.latent_dim, 1))(
                in_layer)
            for n_dim, dim in enumerate(self.hidden_dims[::-1]):
                if n_dim > 0:
                    dec_hidden = klayers.UpSampling1D()(dec_hidden)
                dec_hidden = self.conv_layer(dec_hidden, dim)
                dec_hidden = self.conv_layer(dec_hidden, dim)
            out_layer = self.conv_layer(
                dec_hidden, 1, activation=None, batch_norm=False
            )
            out_layer = klayers.Reshape(target_shape=(self.grid_points, ))(
                out_layer)
            decoder_model = k.Model(
                inputs=in_layer,
                outputs=out_layer
            )
            return decoder_model

    def get_discriminator(self, in_layer):
        with tf.variable_scope('discriminator'):
            in_layer_rshp = klayers.Reshape((self.grid_points, 1))(in_layer)
            disc_hidden = self.single_encoder(in_layer_rshp)
            for n_dim, dim in enumerate(self.hidden_dims[::-1]):
                if n_dim > 0:
                    disc_hidden = klayers.UpSampling1D()(disc_hidden)
                disc_hidden = self.conv_layer(disc_hidden, dim)
            disc_out = self.conv_layer(
                disc_hidden, 1, activation=None, batch_norm=False
            )
            disc_out = klayers.Flatten()(disc_out)
            disc_loss = klayers.Lambda(
                lambda x: self.recon_loss(*x)
            )([in_layer, disc_out])
            disc_model = k.Model(
                inputs=in_layer,
                outputs=disc_loss)
        return disc_model

    def get_forward(self, in_layer):
        with tf.variable_scope('forward'):
            forward_hidden = klayers.Dense(self.hidden_dims[0])(in_layer)
            forward_hidden = klayers.BatchNormalization()(forward_hidden)
            forward_hidden = klayers.Activation('relu')(forward_hidden)
            forward_out = klayers.Dense(self.obs_dim)(forward_hidden)
            forward_model = k.Model(
                inputs=in_layer, outputs=forward_out
            )
        return forward_model

    def get_inverse(self, in_layer):
        with tf.variable_scope('inverse'):
            enc_in_layer = self.encoder.layers[0].output
            inv_hidden = klayers.Dense(self.hidden_dims[0])(in_layer)
            inv_hidden = klayers.BatchNormalization()(inv_hidden)
            inv_hidden = klayers.Activation('relu')(inv_hidden)
            inv_hidden_mean = klayers.Concatenate()(
                [inv_hidden, self.encoder.layers[-2].output])
            inv_hidden_var = klayers.Concatenate()(
                [inv_hidden, self.encoder.layers[-1].output])
            inverse_mean = klayers.Dense(self.latent_dim)(inv_hidden_mean)
            inverse_log_var = klayers.Dense(self.latent_dim)(inv_hidden_var)
            inverse_model = k.Model(
                inputs=[enc_in_layer, in_layer],
                outputs=[inverse_mean, inverse_log_var]
            )
        return inverse_model

    def began_equi(self, gan_real_loss, gan_fake_loss):
        equilib_norm = (self.began['div_ratio'] * gan_real_loss - gan_fake_loss)
        delta_gain = self.began['lr'] * equilib_norm
        update_gain = self.began['gain'].assign_add(delta_gain)
        return equilib_norm, update_gain

    @staticmethod
    def conv_metric(recon_loss, equi_norm):
        return tf.reduce_mean(recon_loss + tf.abs(equi_norm))

    @staticmethod
    def kl_loss(z_mean, z_log_var):
        kl_diff = 1 + z_log_var - tf.square(z_mean) - tf.exp(z_log_var)
        kl_err = - 0.5 * tf.reduce_sum(kl_diff, axis=-1)
        return tf.reduce_mean(kl_err)

    @staticmethod
    def recon_loss(y_real, y_pred):
        absolute_err = tf.abs(y_pred - y_real)
        return tf.reduce_mean(absolute_err)

    def triplet_loss(self, obs, obs_hat, obs_back):
        ana_loss = self.recon_loss(obs, obs_hat)
        loss = ana_loss
        return loss

    def observation_loss(self, obs, obs_hat, obs_back, gan_ana_loss, z_mean,
                         z_log_var):
        triplet_term = self.lam['trip'] * self.triplet_loss(obs, obs_hat,
                                                            obs_back)
        gan_term = self.lam['gan'] * gan_ana_loss
        kl_term = self.lam['kl'] * self.kl_loss(z_mean, z_log_var)
        obs_loss = triplet_term + gan_term + kl_term
        return obs_loss

    def discrim_loss(self, gan_back_loss, gan_ana_loss):
        weighted_floss = self.began['gain'] * gan_ana_loss
        raw_loss = gan_back_loss - weighted_floss
        disc_loss = self.lam['gan'] * raw_loss
        return disc_loss

    def autoencoder_loss(self, z_mean, z_log_var, gan_fake_loss, x_back,
                         x_back_hat, obs, obs_hat, obs_back):
        kl_term = self.lam['kl'] * self.kl_loss(z_mean, z_log_var)
        rec_term = self.lam['rec'] * self.recon_loss(x_back, x_back_hat)
        gan_term = self.lam['gan'] * gan_fake_loss
        triplet_term = self.lam['trip'] * self.triplet_loss(obs, obs_hat,
                                                            obs_back)
        ae_loss = gan_term + triplet_term + rec_term + kl_term
        return ae_loss

    def compile(self):
        ##### AUTOENCODER ######
        self.single_encoder = self.get_single_encoder()
        self.encoder = self.get_encoder(self.inputs['x_back_ens'])
        z_back_mean, z_back_var_log = self.encoder.outputs
        tf.summary.histogram('latent_var', tf.exp(z_back_var_log / 2))
        tf.summary.histogram('latent_mean', z_back_mean)
        z_back_mem = klayers.Lambda(self.sampling, )(
            [z_back_mean, z_back_var_log])
        self.decoder = self.get_decoder(z_back_mem)
        x_back_mem_pred = self.decoder.outputs


        ##### OBSERVATIONS ######
        inv_mean, inv_var_log = self.inverse(
            [self.inputs['x_back_ens'], self.inputs['obs_in']])

        # Kalman update of analysis
        z_back_var = tf.exp(z_back_var_log / 2) 
        inv_var = tf.exp(inv_var_log / 2)
        total_var = z_back_var + inv_var
        z_ana_mean = (z_back_var * z_back_mean + inv_var * inv_mean) / total_var
        z_ana_var_log = tf.log(z_back_var * inv_var / total_var) * 2
        
        # Sample from analysis
        z_ana_mem = klayers.Lambda(self.sampling, )([z_ana_mean, z_ana_var_log])
        x_ana_mem_pred = self.decoder(z_ana_mem)

        # Observation modelling
        self.forward = self.get_forward(self.inputs['x_back_mem'])
        obs_back_pred = self.forward.outputs
        obs_ana_pred = self.forward(x_ana_mem_pred)


        ##### GAN ######
        self.discriminator = self.get_discriminator(self.inputs['x_back_mem'])
        disc_back_loss = self.discriminator.outputs
        disc_ana_loss = self.discriminator(x_ana_mem_pred)


        ##### LOSSES ######
        ae_loss = self.autoencoder_loss(
            z_back_mean, z_back_var_log, disc_ana_loss,
            self.inputs['x_back_mem'], x_back_mem_pred, self.inputs['obs_in'],
            obs_ana_pred, obs_back_pred
        )
        obs_loss = self.observation_loss(
            self.inputs['obs_in'], obs_ana_pred, obs_back_pred, disc_ana_loss,
            z_ana_mean, z_ana_var_log
        )
        disc_loss = self.discrim_loss(disc_back_loss, disc_ana_loss)


        ##### OPTIMIZERS ######
        # Autoencoder
        ae_train_params = self.decoder.trainable_weights + \
                          self.encoder.trainable_weights + \
                          self.single_encoder.trainable_weights
        ae_grad = self.optimizers['ae'].get_gradients(
            ae_loss, ae_train_params
        )
        ae_update = self.optimizers['ae'].get_updates(
            ae_loss, ae_train_params
        )

        # Observations
        obs_train_params = self.forward.trainable_weights + \
                           self.inverse.trainable_weights
        obs_grad = self.optimizers['obs'].get_gradients(
            obs_loss, obs_train_params
        )
        obs_update = self.optimizers['obs'].get_updates(
            obs_loss, obs_train_params
        )

        # GAN
        disc_train_params = self.discriminator.trainable_weights + \
                            self.single_encoder.trainable_weights
        disc_grad = self.optimizers['disc'].get_gradients(
            disc_loss, disc_train_params
        )
        disc_update = self.optimizers['disc'].get_updates(
            disc_loss, disc_train_params
        )

        # BEGAN
        equi_norm, update_gain = self.began_equi(disc_back_loss, disc_ana_loss)


        ##### COMPILATION ######

        self.grads = ae_grad + obs_grad + disc_grad
        self.train_params = ae_train_params + obs_train_params + \
                            disc_train_params
        self.train_steps = [ae_update, obs_update, disc_update, update_gain]
        self.losses = {
            'ae_loss': ae_loss,
            'obs_loss': obs_loss,
            'disc_loss': disc_loss,
        }

        self.metrics = {
            'kl_back': self.kl_loss(z_back_mean, z_back_var_log),
            'kl_ana': self.kl_loss(z_ana_mean, z_ana_var_log),
            'back_rec': self.recon_loss(self.inputs['x_back_mem'],
                                        x_back_mem_pred),
            'diff_ana_back': self.recon_loss(x_back_mem_pred, x_ana_mem_pred),
            'equi_norm': equi_norm,
            'conv_metric': self.conv_metric(disc_back_loss, equi_norm),
            'disc_back_recon': disc_back_loss,
            'disc_ana_recon': disc_ana_loss,
            'obs_back': self.recon_loss(self.inputs['obs_in'], obs_back_pred),
            'obs_ana': self.recon_loss(self.inputs['obs_in'], obs_ana_pred),
            'obs_triplet': self.triplet_loss(
                self.inputs['obs_in'], obs_ana_pred, obs_back_pred
                ),
        }

        self.saver = tf.train.Saver()
        self.summarize()
        self.session.run(tf.global_variables_initializer())

    def summarize(self):
        with tf.name_scope('gradients'):
            for grad in self.grads:
                corrected_name = grad.name.replace(':', '_')
                tf.summary.histogram(corrected_name, grad)
        with tf.name_scope('weights'):
            for weight in self.train_params:
                corrected_name = weight.name.replace(':', '_')
                tf.summary.histogram(corrected_name, weight)
        with tf.name_scope('losses'):
            for name, loss in self.losses.items():
                corrected_name = name.replace(':', '_')
                tf.summary.scalar(corrected_name, loss)
        with tf.name_scope('metrics'):
            for name, loss in self.metrics.items():
                corrected_name = name.replace(':', '_')
                tf.summary.scalar(corrected_name, loss)

        self.summary = tf.summary.merge_all()
        self.train_writer = tf.summary.FileWriter(self.summaries_dir + '/train',
                                                  self.session.graph)
        self.test_writer = tf.summary.FileWriter(self.summaries_dir + '/test')

    def fit_on_batch(self, in_data):
        self.learning = 1
        feed_dict = {self.inputs[key]: value for key, value in in_data.items()}
        _ = self.session.run(
            fetches=self.train_steps,
            feed_dict=feed_dict
        )

    def evaluate_on_batch(self, in_data):
        self.learning = 0
        all_fetches = {
            'losses': self.losses,
            'metrics': self.metrics
        }
        feed_dict = {self.inputs[key]: value for key, value in in_data.items()}
        summary, step_out = self.session.run(
            fetches=[self.summary, all_fetches],
            feed_dict=feed_dict
        )
        return summary, step_out

    def save_model(self, path, **kwargs):
        saved_path = self.saver.save(self.session, path, **kwargs)
        logger.info('Saved model to: {0:s}'.format(saved_path))

    @classmethod
    def load_model(cls, sess, path, **kwargs):
        ens_model = cls(sess)
        ens_model.compile()
        ens_model.saver.restore(sess, path, **kwargs)
        logger.info('Loaded model from: {0:s}'.format(path))
        return ens_model
